import axios from 'axios';

let getPrefixes = (criteria, pageNumber) => {
  const api = axios.create({
    baseURL: 'http://localhost:3000',
    timeout: 5000,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': 'Prefix-Container'
    }
  });

  const perPage = 25;
  
  return api.get(`/phone_records?criteria=${criteria}&page=${pageNumber}&per_page=${perPage}`);
};


export { getPrefixes };