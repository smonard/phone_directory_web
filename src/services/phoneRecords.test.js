import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { getPrefixes } from './phoneRecords';

const mock = new MockAdapter(axios);

const config = {
  baseURL: 'http://localhost:3000',
  timeout: 5000,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': 'Prefix-Container'
  }
};

describe('#getPrefixes', () => {
  it('should call a phone_records endpoint', done => {
    const data = { response: true };

    mock.onGet('/phone_records?criteria=12&page=2&per_page=25', config).reply(200, data);

    getPrefixes(12, 2).then(response => {
      expect(response.data).toEqual(data);
      done();
    });
  });

});

afterEach(() => {
  mock.reset();
})