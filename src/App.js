import React from 'react';
import './App.css';
import Search from './components/searchBox.jsx';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Search/>
      </header>
    </div>
  );
}

export default App;
