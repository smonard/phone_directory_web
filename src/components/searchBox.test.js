import SearchBox from './searchBox.jsx'
import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import * as service from '../services/phoneRecords';

configure({ adapter: new Adapter() });

describe('SearchBox', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<SearchBox />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('updates state when query criteria has changed', () => {
    const expectedQuery = 'some query';
    const searchBox = shallow(<SearchBox />);
    const criteriaInput = searchBox.find('#criteria');

    criteriaInput.simulate('change', { target: { value: expectedQuery } });

    expect(searchBox.instance().state.query).toEqual(expectedQuery)
  });

  it('does not search for phone prefixes when query is empty', () => {
    const searchBox = shallow(<SearchBox />);
    searchBox.instance().state.query = '';
    service.getPrefixes = jest.fn(() => new Promise((resolve, reject) => {}));

    searchBox.instance().handleSubmit({ preventDefault: () => { } });

    expect(service.getPrefixes.mock.calls[0]).toBe(undefined);
  });

  describe('ignores (0)', () => {
    const searchBox = shallow(<SearchBox />);

    beforeEach(() => {
      service.getPrefixes = jest.fn(() => new Promise((resolve, reject) => {}));
    });

    it('ignores (0) in the query', () => {
      searchBox.instance().state.query = ' 141(0)114';
  
      searchBox.instance().handleSubmit({ preventDefault: () => { } });
      
      expect(service.getPrefixes.mock.calls[0][0]).toBe('141114');
    });

    it('ignores (0) in the query with leading blankspace', () => {
      searchBox.instance().state.query = ' 141 (0)114';
  
      searchBox.instance().handleSubmit({ preventDefault: () => { } });
      
      expect(service.getPrefixes.mock.calls[0][0]).toBe('141114');
    });

    it('ignores (0) in the query with trailing blankspace', () => {
      searchBox.instance().state.query = ' 141(0) 114';
  
      searchBox.instance().handleSubmit({ preventDefault: () => { } });
      
      expect(service.getPrefixes.mock.calls[0][0]).toBe('141114');
    });

    it('ignores (0) in the query with leading and trailing blankspaces', () => {
      searchBox.instance().state.query = ' 141 (0) 114';
  
      searchBox.instance().handleSubmit({ preventDefault: () => { } });
      
      expect(service.getPrefixes.mock.calls[0][0]).toBe('141114');
    });

  });
  

  describe('SearchBox#updatePrefixes', () => {
    let responseMock = {
      data: [{ object: true }],
      headers: {
        total: 51
      }
    };

    it('updates state when page number has changed', () => {
      const searchBox = shallow(<SearchBox />);
      searchBox.instance().state.query = 'somequery';
      service.getPrefixes = jest.fn(() => new Promise((resolve, reject) => resolve(responseMock)));

      searchBox.instance().handlePageChange(2);

      service.getPrefixes().then(() => {
        expect(searchBox.instance().state.pages.current).toEqual(2);
        expect(searchBox.instance().state.pages.total).toEqual(3);
      });

    });

    it('updates state when form has been submitted', () => {
      service.getPrefixes = jest.fn(() => new Promise((resolve, reject) => resolve(responseMock)));
      const searchBox = shallow(<SearchBox />);
      searchBox.instance().state.query = 'somequery';

      searchBox.find('#main-form').simulate('submit', { preventDefault: () => { } });

      service.getPrefixes().then(() => {
        expect(searchBox.instance().state.list).toEqual(responseMock.data);
        expect(searchBox.instance().state.pages.total).toEqual(3);
      });
    });
  });
});