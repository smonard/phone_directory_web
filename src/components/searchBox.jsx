import React, { Component } from 'react';
import PhoneRecordList from './phoneRecords';
import { getPrefixes } from '../services/phoneRecords';
import './search-box.css';

class Search extends Component {

  constructor(props) {
    super(props);
    this.state = { query: '', list: [], pages: { current: 1, total: 0 } };

    this.handleQueryChange = this.handleQueryChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
    this.updatePrefixes = this.updatePrefixes.bind(this);
    this.cleanQuery = this.cleanQuery.bind(this);
  }

  updatePrefixes(query, page) {
    query = this.cleanQuery(query);
    if (query !== '') {
      getPrefixes(query, page).then((response) => {
        const pagesInfo = {
          total: Math.floor((response.headers.total / 25) + 1) || 0,
          current: page
        };
        this.setState({ query: query, list: response.data, pages: pagesInfo });
      }).catch(() => { });
    }
  }

  cleanQuery(query) {
    [' (0) ',' (0)','(0) ','(0)'].map(zero => query = query.replace(zero, ''));
    return query.trim();
  }

  handleQueryChange(event) {
    this.setState({ query: event.target.value });
  }

  handleSubmit(event) {
    this.updatePrefixes(this.state.query, 1);
    event.preventDefault();
  }

  handlePageChange(newPage) {
    this.updatePrefixes(this.state.query, newPage);
  }

  render() {
    return (
      <div className="Container" >
        <form id='main-form' onSubmit={this.handleSubmit}>
          <label>Query:
            <input id="criteria" type="text" value={this.state.query} onChange={this.handleQueryChange} />
          </label>
          <input id="submit-button" type="submit" value="Submit" />
        </form>
        <div>
          <PhoneRecordList list={this.state.list} />
          <div>
            <ul id='page-numbers'>
              {[...Array(this.state.pages.total).keys()].map((number) => (
                <li key={`page-${number}`} id={`page-${number}`} onClick={() => this.handlePageChange(number + 1)} className={(this.state.pages.current === (number + 1) ? 'current-page' : 'page')}>
                  <a>{number + 1}</a>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>

    );
  }
}

export default Search;