import PhoneRecords from './phoneRecords.jsx'
import React from 'react';
import ReactDOM from 'react-dom';
import { mount, shallow } from 'enzyme';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

describe('PhoneRecords', () => {

  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<PhoneRecords list={[]} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('renders the right phone prefix records number', () => {
    const mockList = [{ id: 1 }, { id: 2 }];
    const phoneRecords = shallow(<PhoneRecords list={mockList} />);

    const actualRecords = phoneRecords.find('.record');
    expect(actualRecords.length).toBe(2);
  });

  it('renders the right phone prefix record information', () => {
    const mockList = [{ id: 1, prefix: 512, usage: 'Bre', comment: 'comment' }];
    const phoneRecords = shallow(<PhoneRecords list={mockList} />);
    const actualRecords = phoneRecords.find('#record-0');
    expect(actualRecords.html()).toEqual('<tr id=\"record-0\" class=\"record\"><td id=\"prefix-0\">512</td><td id=\"min_len-0\"></td><td id=\"max_len-0\"></td><td id=\"name-0\">comment</td><td id=\"usage-0\">Bre</td></tr>');
  });
});