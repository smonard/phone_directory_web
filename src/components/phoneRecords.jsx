
import React, { Component } from 'react'
import './phone-records.css'

class PhoneRecordsList extends Component {

    constructor(props) {
        super(props);
        this.state = { list: props.list };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ list: nextProps.list });
    }

    render() {
        return (
            <div>
                <table className="main-table">
                    <thead>
                        <tr>
                            <th scope="col">Prefix</th>
                            <th scope="col">Minimum length</th>
                            <th scope="col">Maximum length</th>
                            <th scope="col">Name</th>
                            <th scope="col">Usage</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.list.map((item, index) => (
                            <tr key={item.id} id={`record-${index}`} className="record">
                                <td id={`prefix-${index}`}>{item.prefix}</td>
                                <td id={`min_len-${index}`}>{item.min_len}</td>
                                <td id={`max_len-${index}`}>{item.max_len}</td>
                                <td id={`name-${index}`}>{item.comment}</td>
                                <td id={`usage-${index}`}>{item.usage}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default PhoneRecordsList;