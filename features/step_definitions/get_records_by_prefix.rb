require 'selenium-webdriver'

Before do
    @driver = Selenium::WebDriver.for :firefox
    @driver.navigate.to "http://localhost:8080/"
end

Given("there are records matching the {int} prefix") do |prefix|
    curl_result = system "curl http://localhost:3000/phone_records -s | grep -c \"#{prefix}\" > /dev/null"
    expect(curl_result).to eq(true) # evals the exit code of curl|grep
end

When("I enter the prefix {int}") do |prefix|
    enter_text prefix
end

Then("I should see a list with the matching records: {int}") do |records_count|
    wait = Selenium::WebDriver::Wait.new(timeout: 5) # seconds
    wait.until { @driver.find_element(class: "record") }
    result_count = @driver.find_elements(class: 'record').count
    expect(result_count).to eq(records_count)
end


Given("there are records matching the Tel") do
end
  
When("I enter the prefix Tel") do
    enter_text 'Tel'
end
  
Given("there are records matching the {int} prefix and Frank as name") do |int|                                                         
end
  
When("I enter the prefix {int} and Frank as name") do |prefix|
    enter_text "#{prefix} Frank"
end
  
When("I enter the following search criteria: {int} Frank Berl {int}") do |prefix, prefix2|                                                  
    enter_text "#{prefix} Frank Berl #{prefix2}"
end
  
When("I enter the following search criteria: Rösrath") do
    enter_text 'Rösrath'                                               
end
  
Then("I see the correct values are shown") do
    expect(@driver.find_element(id: 'prefix-0').text).to eq('2205')
    expect(@driver.find_element(id: 'name-0').text).to eq('Rösrath')
    expect(@driver.find_element(id: 'usage-0').text).to eq('Geographic Area Code')
    expect(@driver.find_element(id: 'min_len-0').text).to eq('6')
    expect(@driver.find_element(id: 'max_len-0').text).to eq('11')

end
      
def enter_text(text)
    criteria_input = @driver.find_element(id: 'criteria')
    criteria_input.send_keys "#{text}"
    criteria_input.submit
end
