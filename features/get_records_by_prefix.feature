Feature: Get records by prefix
  Should return a list of records matching the desired prefix

  Scenario: There are matching records when searching by prefix
    Given there are records matching the 15888 prefix
    When I enter the prefix 15888
    Then I should see a list with the matching records: 1

  Scenario: There are matching records when searching by name
    Given there are records matching the Tel
    When I enter the prefix Tel
    Then I should see a list with the matching records: 17


  Scenario: There are matching records when searching by prefix and name
    Given there are records matching the 19890 prefix and Frank as name
    When I enter the prefix 19890 and Frank as name
    Then I should see a list with the matching records: 12


  Scenario: There are matching records when searching by various names and prefixes
    When I enter the following search criteria: 21294 Frank Berl 895
    Then I should see a list with the matching records: 15

  Scenario: Supports umlauts
    When I enter the following search criteria: Rösrath
    Then I should see a list with the matching records: 1
    And I see the correct values are shown