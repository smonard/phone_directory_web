# Phone Directory Web

User interface for phone directory searching.

## Useful Scripts

In the project directory, you can run:


### `npm install`

It will install all the necessary dependencies to run the app.

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Running E2E tests

First you may need to setup your environment:

    bundle install --path vendor/bundle

Also, you must download the geckodriver: https://github.com/mozilla/geckodriver/releases

This is used to control firefox, and must be either in your path (PATH) or in your binaries directory. Run geckodriver anywhere and should work.

Remember that before running e2e tests, the entire system must be up and running (api + web + db).

#### Command to run E2E tests:

    bundle exec cucumber





This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).